.PHONY = open clean

dist/notes.pdf: dist dist/notes.tex dist/ch01.tex dist/ch02.tex dist/ch03.tex dist/ch04.tex dist/ch05.tex dist/ch06.tex dist/ch07.tex dist/ch08.tex
	cd dist && latexmk -xelatex notes.tex

dist:
	mkdir -p dist

dist/notes.tex: dist notes.tex
	cp notes.tex dist/

dist/ch01.tex: dist src/1\ -\ Fehleranalyse\ und\ Computerzahlen.md
	pandoc "src/1 - Fehleranalyse und Computerzahlen.md" -o dist/ch01.tex

dist/ch02.tex: dist src/2\ -\ Vektorräume\ und\ Matrizen.md
	pandoc "src/2 - Vektorräume und Matrizen.md" -o dist/ch02.tex

dist/ch03.tex: dist src/3\ -\ Lineare\ Gleichungssysteme.md
	pandoc "src/3 - Lineare Gleichungssysteme.md" -o dist/ch03.tex

dist/ch04.tex: dist src/4\ -\ Lineare\ Quadratmittelprobleme.md
	pandoc "src/4 - Lineare Quadratmittelprobleme.md" -o dist/ch04.tex

dist/ch05.tex: dist src/5\ -\ Polynome.md
	pandoc "src/5 - Polynome.md" -o dist/ch05.tex

dist/ch06.tex: dist src/6\ -\ Polynominterpolation.md
	pandoc "src/6 - Polynominterpolation.md" -o dist/ch06.tex

dist/ch07.tex: dist src/7\ -\ Quadratur.md
	pandoc "src/7 - Quadratur.md" -o dist/ch07.tex

dist/ch08.tex: dist src/8\ -\ Nichtlineare\ Gleichungen.md
	pandoc "src/8 - Nichtlineare Gleichungen.md" -o dist/ch08.tex

open: dist/notes.pdf
	xdg-open dist/notes.pdf

clean:
	rm -rf dist
