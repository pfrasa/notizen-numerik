---
previous_sections: 5
---

# Polynominterpolation

In diesem Kapitel untersuchen wir, wie wir anhand einer Menge an Datenpaaren $(x_i, y_i)$ ein Polynom $p$ finden können, das diese **interpoliert**, d.h. für das jeweils $p(x_i) = y_i$ gilt.

## Zusammenfassung {-}

## Problemstellung
Gegeben sei ein System von $n+1$ verschiedenen **Stützstellen** $\Delta := \{x_k\}_{k=0}^n$ , sowie zugehörige **Stützwerte** $y_k$. Wir suchen nun ein interpolierendes Polynom $p$ aus $\Pi_n$. Ist eine Basis für $\Pi_n$ gegeben, so können wir das Problem durch Aufstellen eines Gleichungssystems lösen, um eine Darstellung von $p$ in Bezug auf die gegebene Basis zu erreichen. Da wir $n+1$ Bedingungen und $n+1$ Basiselemente haben, liegt es nahe, daß dieses Problem grundsätzlich lösbar ist. Daß dem so ist, zeigen wir formal im nächsten Abschnitt. Ebenso werden wir systematischere bzw. effizientere Verfahren kennenlernen, um das Interpolationspolynom zu bestimmen oder es an einer bestimmten Stelle auszuwerten.

## Die Lagrange'sche Darstellung
Ist ein System von Stützstellen wie oben gegeben, so definieren wir jeweils

$$
L_k(x) = \prod_{j=0,j\neq k}^n \frac{x-x_j}{x_k-x_j},\quad k=0,\ldots,n
$$

Diese $n+1$ Polynome sind alle vom Grad $n$ und bilden eine Basis von $\Pi_n$. Sie werden **Lagrange'sche Basispolynome** genannt. Man sieht sofort, daß $L_k(x_j) = \delta_{kj}$ gilt, die Polynome nehmen also jeweils in genau einer Stützstelle den Wert $1$ an und verschwinden in allen anderen Stützstellen.

Ferner definieren wir $\omega_{n+1}(x)=(x-x_0)\cdots(x-x_n)$ als das zu den Stützstellen zugehörige **Knotenpolynom**. Es hat genau die Stützstellen als Nullstellen und den Leitkoeffizienten $1$.

Man sieht nun, daß

$$
p_n(x) = \sum_{k=0}^n y_kL_k(x)
$$

ein Polynom vom Grad $n$ ist, das die Interpolationsaufgabe löst. Wie wir im letzten Kapitel schon gezeigt haben, ist diese Lösung auch eindeutig.

Die Aufgabenstellung, das Interpolationspolynom bezüglich dieser Basis zu bestimmen, ist also trivial zu lösen: Die Koeffizientenmatrix des Gleichungssystem ist die Identitätsmatrix. Allerdings bestehen die folgenden Nachteile:

- Es ist deutlich aufwändiger, eine Darstellung z.B. bezüglich der Monombasis zu gewinnen: Polynome müssen ausmultipliziert und nach Potenzen sortiert werden.
- Ändert man nur eine einzige Stützstelle, so ändert sich die gesamte Basis.

Deswegen sind die Lagrange'schen Basispolynome eher aus theoretischer Sicht von Interesse.

## Die Newton'sche Darstellung
Die Idee der Newton'schen Darstellung besteht darin, eine Basis zu finden, bei der die Koeffizientenmatrix des zu lösenden Gleichungssystems eine Dreiecksmatrix ist. Damit läßt sich das System leicht lösen.

Ist wieder ein System von Stützstellen gegeben, so definieren wir
$$
N_0(x) = 1,\quad N_k(x) = (x-x_0)(x-x_1)\cdots(x-x_{k-1}).
$$
Diese Polynome bilden wieder eine Basis von $\Pi_n$, wobei nun aber $N_k$ jeweils den Grad $k$ hat. Diese werden **Newton'sche Basispolynome** genannt.

Wie erwähnt, läßt sich die Problemstellung nun durch Aufstellen eines Gleichungssystems lösen, wobei die Koeffizientenmatrix eine untere Dreiecksmatrix ist. Dies können wir aber auch systematischer darstellen. So hat das gesuchte Polynom die Form
$$
p_n(x) = y[x_0]N_0 + y[x_0,x_1]N_1 + \cdots + y[x_0,\cdots,x_n]N_n,
$$
wobei $y[x_0,\cdots,x_i]$ jeweils der Leitkoeffizient des eindeutig bestimmten Polynoms aus $\Pi_i$ ist, das $(x_0,y_0),\cdots,(x_i,y_i)$ interpoliert. Dabei gilt auch die folgende Rekursion:

\begin{align*}
y[x_k] &= y_k\\
y[x_k,\cdots,x_{k+j}] &= \frac{y[x_{k+1},\cdots,x_{k+j}]-y[x_k,\cdots,x_{k+j-1}]}{x_{k+j}-x_k}
\end{align*}

Entsprechend bezeichnet man die $y[x_k,\cdots,x_{k+j}]$ auch als **dividierte Differenzen**. Für die Berechnung der entsprechenden Werte nutzen wir das sogenannte **Steigungsschema** als graphische Darstellung.

Der große Vorteil der Newton'schen Darstellung ist, daß man weitere Stützstellen und -werte relativ einfach ergänzen kann.

## Hermite-Interpolation

Eine Verallgemeinerung der Polynominterpolation in Newton'scher Darstellung ist die Hermite-Interpolation, bei der auch Ableitungswerte berücksichtigt werden. Speziell gibt es für jedes $x_i$ ein $r_i \geq 0$, so daß wir jeweils die Bedingungen
$$
p^{(k)}(x_i) = y_{k,i},\quad k=0,\cdots,r_i
$$
haben.[^1] Haben wir so insgesamt $n+1$ Bedingungen aufgestellt, so zeigt sich, daß es ein eindeutig definiertes Polynom in $\Pi_{n}$ gibt, das diese erfüllt.

[^1]: Es dürfen insbesondere keine Ableitungen zwischendrin "übersprungen" werden.

Zur Bestimmung des entsprechenden Polynoms können wir wieder das Steigungsschema bzw. die entsprechende Rekursion verwenden, hierbei haben wir dann aber Ausdrücke wie $y[x_0,x_0,x_0,x_1,x_1]$, wenn wir in $x_0$ zwei Ableitungswerte und in $x_1$ einen berücksichtigen. Zudem müssen wir $y[x_i,\cdots,x_i]=\frac{y_{n,i}}{n!}$ definieren, um die Division durch $0$ zu vermeiden -- wobei $x_i$ im Ausdruck $n+1$-mal vorkommt.

Die Darstellung des Polynoms ist dann bspw. durch
\begin{align*}
p(x) = &y[x_0] + y[x_0,x_0](x-x_0) + y[x_0,x_0,x_0](x-x_0)^2\\+&y[x_0,x_0,x_0,x_1](x-x_0)^3 + y[x_0,x_0,x_0,x_1,x_1](x-x_0)^3(x-x_1)
\end{align*}
gegeben.

## Auswertung des Interpolationspolynoms

Will man das Interpolationspolynom an einer bestimmten Stelle auswerten, so gibt es dafür mehrere Verfahren.

Beim **Verfahren von Aitken und Neville** wird der Funktionswert direkt anhand von Stützstellen und -werten ausgewertet, ohne zuerst eine allgemeine Darstellung des Polynoms bestimmen müssen. Dafür definieren wir zunächst $p_{m,k}$ als dasjenige Polynom aus $\Pi_m$, das die gesuchte Funktion in den Stützstellen $x_{k-m},\cdots,x_k$ interpoliert. Wir gewinnen dann die rekursive Darstellung
\begin{align*}
p_{0,k}(x) &= y_k\\
p_{m,k}(x) &= \frac{(x-x_{k-m})p_{m-1,k}(x)-(x-x_k)p_{m-1,k-1}(x)}{x_k-x_{k-m}},
\end{align*}
aus der sich ein Algorithmus ableiten läßt. Offensichtlich ist dann $p_{n,n}$ das gesuchte Polynom. Graphisch stellt man das Verfahren durch ein modifiziertes Steigungsschema, das **Neville-Schema** dar.

Beim **modifizierten Horner-Schema** ist hingegen eine Darstellung bezüglich der Newton-Basis bereits gegeben. Man kann nun, wie beim Horner-Schema, sukzessive die Koeffizienten ausklammern, um dann in einem ähnlichen Verfahren das Polynom sehr effizient an einer bestimmten Stelle auszuwerten. Allerdings muß dafür erst das Polynom bestimmt werden -- wird dieses aber an mehreren Stellen ausgewertet, so ist dieses Verfahren gegenüber dem von Aitken und Neville vorzuziehen.

## Interpolationsfehler

Wir berücksichtigen nun den Approximationsfehler, wenn wir eine Funktion $f$ durch ein Interpolationpolynom $p \in \Pi_n$ mit $n+1$ Stützstellen darstellen. Ist $f \in C^{n+1}[a,b]$, so existiert für jedes $x\in [a,b]$ ein *von $x$ abhängiges* $\xi \in (a,b)$, für das
$$
f(x)-p(x) = \frac{f^{(n+1)}(\xi)}{(n+1)!}\omega_{n+1}(x)
$$
gilt, wobei $\omega_{n+1}$ das Knotenpolynom zu den Stützstellen ist. Der Fehler hängt also offensichtlich von der Lage der Stützstellen ab. Daraus leitet man direkt ab, daß der Leitkoeffizient des Interpolationspolynoms den Wert $\frac{f^{(n)}(\xi)}{n!}$ für ein $\xi$ zwischen der kleinsten und der größten Stützstelle hat.

Für die Hermite-Interpolation können wir ebenfalls eine Fehlerabschätzung geben: Haben wir $n+1$ Interpolationsbedingungen so existiert für jedes $x$ ein $\xi$ mit
$$
f(x)-p(x) = \frac{f^{(n+1)}(\xi)}{(n+1)!}\prod (x-x_i)^{1+r_i}
$$

## Das Runge-Phänomen
Man würde vermuten, daß der Interpolationsfehler abnimmt, je mehr Stützstellen wir benutzen. Daß dem nicht unbedingt so ist, zeigt die **Runge-Funktion**
$$
f_R(t) = \frac{1}{1+25t^2}
$$
auf dem Intervall $[-1,1]$. Verwenden wir jeweils $n$ gleichabständige Stützstellen, so wird der der Approximationsfehler an den Rändern immer größer.

Im allgemeinen Fall kann man sogar zeigen, daß es keine Folge von Stützstellen gibt, für die der Interpolationsfehler für zunehmendes $n$ überall gegen $0$ konvergiert, d.h. für jede solche Folge können wir eine stetige Funktion $f$ finden, die ein ähnlich schlechtes Verhalten zeigt. Im nächsten Abschnitt zeigen wir aber, daß dieses Problem bei hinreichend glattem $f$ nicht mehr auftritt.

## Optimierung der Stützstellen
Wie wir gesehen haben, hängt der Interpolationsfehler bei hinreichend glattem $f$ von der Lage der Stützstellen ab. Dabei ist die Maximumnorm des Knotenpolynoms auf dem Intervall $[a,b]$ genau dann minimal, wenn wir als Stützstellen die Nullstellen des Tschebyscheff-Polynoms verwenden: also ist diese Wahl optimal. Ist $[a,b]=[-1,1]$, so ist diese Norm z.B. gleich $2^{-n}$. Damit tritt das Runge-Phänomen nicht mehr auf.

## Rundungsfehlereinfluß
Neben dem Interpolationsfehler wollen wir nun auch Fehler untersuchen, die durch Fehler in den Berechnungen der Stützwerte zustandekommen. So seien zum Beispiel alle $y_k$ durch einen Fehler $\delta_k$ mit $|\delta_k| \leq \delta$ gestört. Das "korrekte" Interpolationspolynom wollen wir mit $p$ und das durch die gestörten Werte berechnete mit $\widetilde{p}$ bezeichnen. Dann gilt
$$
|\widetilde{p}(x)-p(x)| \leq \delta\lambda_\Delta(x),
$$
mit der zu dem Stützstellensystem zugehörigen **Lebesgue-Funktion** $\lambda_\Delta(x) := \sum_{k=0}^n|L_k(x)|$.

Durch Übergang zum Maximum über einem Intervall $[a,b]$ gilt für alle $x \in [a,b]$
$$
\lVert \widetilde{p}-p\rVert_{[a,b]} \leq \delta\Lambda_\Delta
$$
mit der (vom Intervall abhängigen) **Lebesgue-Konstanten** $\Lambda_\Delta := \lVert \lambda_\Delta \rVert_{[a,b]}$.

## Lernziele {-}

### Definitionen und Sätze {-}
- Polynominterpolation: Problemstellung
- Existenz (und Eindeutigkeit) des Interpolationspolynoms
- Lagrange'sche Basispolynome, Knotenpolynom, Zusammenhang
- Darstellung über Lagrange'sche Basispolynome
- Newton'sche Basispolynome und Darstellung des Interpolationspolynoms
- Definition der $y[x_i,\cdots,x_k]$, rekursive Formel
- Hermite-Interpolation
- Interpolationsfehler, Leitkoeffizient des Interpolationspolynoms
- Runge-Funktion, Runge-Phänomen
- Tschebyscheff-Nullstellen sind optimale Stützstellen

### Verfahren {-}
- Bestimmen des Interpolationspolynoms:
    - Lösen eines lineares Gleichungssystems bei allgemeiner Basis
    - Darstellung bzgl. Lagrange-Basis (ggf. ausmultiplizieren zu Monom-Basis)
    - Darstellung bzgl. Newton-Basis: untere Dreiecksmatrix, Rekursion über Steigungsschema
 - Bestimmen des Hermite-Interpolationspolynoms: Modifikation Steigungsschema
 - Auswertung des Polynoms:
     - Aitken-Neville
     - modifiziertes Horner-Schema
    
### Beweise {-}

- Satz 6.2.3: Existenz einer Lösung (über Lagrange-Polynome)
- Satz 6.3.6: Koeffizienten der Newton-Darstellung sind Leitkoeffizienten von Interpolationspolynomen
- Satz 6.3.7: Rekursionsformel für Newton-Koeffizienten
- Satz 6.4.1: Rekursionsformel im Verfahren von Aitken und Neville
- Sätze 6.5.1, 6.5.3: Interpolationsfehler und Anwendung
- Abschnitt 6.7: Herleitung Rundungsfehler
- Satz 6.9.1: Tschebyscheff-Nullstellen sind optimale Stützstellen

### Aufgaben {-}

- 6.2.5: Summe der Lagrangepolynome ist $1$
- 6.4.7, 6.6.2: Anwendung Steigungsschema und modifiziertes Horner-Schema
- Studientag:
    - 14: Andere Darstellung dividierte Differenzen (Induktion)
- EAs?