---
previous_sections: 1
---
# Vektorräume und Matrizen

## Zusammenfassung {-}

## Normierte Räume
Wir wiederholen die Begriffe einer **Norm** bzw. eines normierten Raumes. Die wichtigsten Normen auf $\mathbb{C}^n$ sind für uns zunächst einmal die *p-Normen*

$$
\lVert x \rVert_p := \left( \sum_{i=1}^n |x_i|^p\right)^{1/p},\quad p\geq 1
$$

mit den Spezialfällen der *Summennorm* ($p=1$), *euklidischen Norm* ($p=2$) und der *Maximumnorm* ($p\to\infty$).

Auch auf dem Vektorraum stetiger Funktionen $C[a,b]$ lassen sich Normen definieren, z.B. die sogenannten $L^p$-Normen und die Maximumnorm:

$$
\lVert f \rVert_p := \left(\int_a^b |f(s)|^p \mathrm{d}s\right)^{1/p},\quad p\geq 1,\quad\quad \lVert f \rVert_\infty := \max |f(s)|
$$

Auf *endlichdimensionalen* Räumen sind alle Normen äquivalent, im Sinne daß es für $\lVert.\rVert$ und $\lVert.\rVert_\ast$ zwei Konstanten $A, B > 0$ gibt mit $A\lVert x \rVert \leq \lVert x \rVert_\ast \leq B\lVert x\rVert$ für alle $x$. Dies gilt für *unendlichdimensionale* Räume wie $C[a,b]$ allerdings nicht.

## Skalarprodukt

Wir wiederholen den Begriff eines **Skalarpoduktes** $(x,y)\mapsto \langle x,y\rangle$ als positiv definite Sesquilinearform (positiv definite symmetrische Bilinearform im Falle von $\mathbb{R}$) und euklidischer bzw. unitärer Vektorräume. Ist ein Skalarprodukt gegeben, so ist
$$
\lVert x \rVert := \sqrt{\langle x,x \rangle}
$$
die dadurch **induzierte Norm**

Für das Skalarprodukt und die dadurch induzierte Norm gilt stets die *Cauchy-Schwarz'sche Ungleichung*:
$$
|\langle x, y \rangle| \leq \lVert x \rVert \lVert y \rVert
$$

Die wichtigsten Skalarprodukte für uns sind:

- $\langle x, y \rangle := y^H x = \sum_{i=1}^n x_i\overline{y_i}$ auf $\mathbb{C}^n$, das die euklidische Norm induziert
- $\langle f, g \rangle := \int_a^b fg$ auf $C[a,b]$, das die $L^2$-Norm induziert

Bei der Berechnung des Skalarproduktes $y^Hx$ diskutieren wir, wie uns die parallele Verarbeitung von Algorithmen einen Berechnungsvorteil geben kann: wir können nämlich die zu berechnende Summe in Teilsummen aufteilen, diese von verschiedenen Prozessen oder Threads berechnen lassen und das Ergebnis wieder im Hauptthread zusammenführen. Allgemein können wir einen parallelen Algorithmus oft durch die Phasen **Initialisierungphase**, **parallele Phase** und **Reduktionsphase** beschreiben.

## Orthogonalität

Zwei Vektoren $x, y$ heißen orthogonal, wenn $\langle x, y \rangle = 0$ gilt. Zu einem Unterraum $U$ eines unitären Vektorraumes $V$ kann man das **orthogonale Komplement** $U^\perp$ bilden, das alle Vektoren enthält, die zu allen Vektoren in $U$ orthogonal sind. Auch $U^\perp$ ist ein Unterraum von $V$ und es gilt $U \oplus U^\perp = V$.

Ist eine linear unabhängige Menge $\{x_1, \ldots, x_n\}$ von Vektoren gegeben, so lässt sich eine **orthonormale** Menge von Vektoren $\{u_1,\ldots,u_n\}$ bilden, die denselben Raum aufspannt, und deren Vektoren alle orthogonal zueinander sind und jeweils die Norm $1$ haben. Diese Konstruktion läßt sich durch Das *Gram-Schmidt-Verfahren* ausführen, bei dem wir auch eine modifizierte Variante besprechen, die numerisch stabiler ist. Allerdings ist selbst bei der modifizierten Variante der Algorithmus nur für eine geringe Anzahl Vektoren brauchbar: die Skalarprodukte zwischen den Vektoren entfernen sich sonst zu stark von der $0$. Das *Householder-Verfahren* aus Kapitel 4 ist deswegen in der Praxis geeigneter.

## Normen linearer Abbildungen
Bekanntermaßen ist die Menge $\mathrm{Hom}(V,W)$ aller linearen Abbildungen zwischen zwei Vektorräumen $V$ und $W$ selber ein Vektorraum. Wir nennen eine lineare Abbildung $L$ zwischen zwei normierten Räumen **beschränkt**, wenn ein $C$ existiert, für das $\lVert Lx \rVert \leq C \Vert x \rVert$ für alle $x$ gilt. Die Menge $\mathcal{L}(V,W)$ aller beschränkten Abbildungen ist ein Unterraum von $\mathrm{Hom}(V,W)$. Auf diesem lässt sich die Norm
$$
\lVert L \rVert := \sup_{x\neq0} \frac{\lVert Lx \rVert}{\lVert x \rVert} = \sup_{x, \lVert x \rVert=1} \lVert Lx \rVert
$$
definieren.

Ist $V = W$, so sprechen wir vom Raum der Endomorphismen auf $V$. In diesem Falle lässt sich mit der Komposition $L \circ K$ von Abbildungen eine Multiplikation einführen. In diesem Zusammenhang nennen wir eine Norm auf $\mathcal{L}(V,V)$ **submultiplikativ**, wenn $\lVert L\circ K\rVert \leq \lVert L \rVert \lVert K \rVert$ gilt. Weiter nennen wir die Norm **verträglich** mit der Norm auf $V$, falls gilt $\lVert Lx \rVert \leq \lVert L \rVert \lVert x \rVert$ für alle $x$. Die Norm
$$
\lVert L \rVert := \sup_{x\neq0} \frac{\lVert Lx \rVert}{\lVert x \rVert}
$$
wird die der Norm auf $V$ **zugeordnete Norm** genannt und ist submultiplikativ und mit der Norm auf $V$ verträglich.

Da Matrizen beschränkte lineare Abbildungen repräsentieren, können wir all diese Begriffe auch auf Matrizen anwenden. Eine submultiplikative Norm auf einem Matrizenraum wird dabei auch eine **Matrixnorm** genannt: nicht jede Norm auf $\mathbb{R}_{n,m}$ ist auch eine Matrixnorm.

Wir lernen eine Reihe wichtiger Matrixnormen kennen:

- Die **Zeilensummennorm** $\lVert A \rVert_\infty := \max_{i} \sum_{j=1}^n |a_{ij}|$ ist der Maximumnorm zugeordnet
- Die **Spaltensummennorm** $\lVert A \rVert_1 := \max_j \sum_{i=1}^n |a_{ij}|$ ist Summennorm zugeordnet
- Die **Spektralnorm** $\lVert A \rVert_2 := \max\{\sqrt{\lambda} : \lambda\text{ ist Eigenwert von }A^HA\}$ ist der euklidischen Norm zugeordnet
- Die **Frobeniusnorm** $\lVert A \rVert_F = \sqrt{\sum_{i,j=1}^n |a_{ij}|^2}$ ist mit der euklidischen Norm verträglich

Abschließend beweisen wir das **Störungslemma**. Dies besagt, daß wenn eine invertierbare Matrix durch eine kleine Störung betroffen ist, die gestörte Matrix immer noch invertierbar ist. Sei spezifischer $A$ invertierbar und $\Delta_A$ eine Matrix mit $\lVert A^{-1}\Delta_A\rVert < 1$. Dann ist $A+\Delta_A$ invertierbar und es gilt
$$
\lVert (A+\Delta_A)^{-1} \rVert \leq \frac{\lVert A^{-1} \rVert}{1-\lVert A^{-1}\Delta_A \rVert}.
$$

## Spezielle Matrizen
In diesem Abschnitt definieren wir einige spezielle Arten von Matrizen.

Wir erinnern an die Begriffe der oberen bzw. unteren **Dreiecksmatrizen** und der **Diagonalmatrix**. Ist nun $l^i$ ein Vektor, dessen erste $i$ Komponenten $0$ sind und $e^i$ der $i$-te Standardbasisvektor, so nennen wir
$$
L_i = I_n + l^i(e^i)^H = \begin{pmatrix}1 & & & & 0\\& \ddots & & &\\& & 1 & &\\& & l^i_{i+1} &  &\\& & \vdots & \ddots &\\0 & & l^i_n & & 1\end{pmatrix}
$$
eine **elementare untere Dreiecksmatrix**. Es gilt $L_i^{-1} = I_n - l^i(e^i)^H$. Weiterhin läßt sich jede untere $n\times n$- Dreiecksmatrix mit normierter Diagonale als Produkt von $n-1$ elementaren unteren Dreicksmatrizen schreiben.

**Permutationsmatrizen** sind Matrizen, die in jeder Zeile und jeder Spalte genau eine $1$ und sonst überall eine $0$ haben. Permutationsmatrizen vertauschen die Zeilen bzw. Spalten (je nach Multiplikationsrichtung) anderer Matrizen. **Vertauschungsmatrizen** vertauschen nur genau zwei Zeilen bzw. Spalten und sind von der Form $T_{ij} = I - (e^i-e^j)(e^i-e^j)^H$.

Für Vertauschungsmatrizen und elementare untere Dreiecksmatrizen gilt folgender Satz: Ist $T_{ij}$ eine Vertauschungsmatrix und $L = I_n+l^{k}(e^k)^H$ eine elementare untere Dreiecksmatrix, so ist $T_{ij}L = \widetilde{L}T_{ij}$ für $\widetilde{L} = I_n + T_{ij}l^k(e^k)^H$.

Eine Matrix heißt **Hermite'sch**, wenn $A^H = A$ gilt (im reellen Fall sind dies also symmetrische Matrizen). Gilt für eine Hermite'sche Matrix $x^HAx \geq 0$ für alle $x$, so heißt $A$ positiv semidefinit. Ist weiterhin $x^H A x > 0$ für alle $x\neq 0$, so ist $A$ positiv definit.[^1]

[^1]: Wir erinnern daran, daß Sesquilinearformen durch Hermite'sche Matrizen bzw. symmetrische Bilinearformen durch symmetrische Matrizen anhand der Beziehung $\langle x, y \rangle = y^H A x$ dargestellt werden können.

Eine Matrix heißt **diagonal dominant**, falls in jeder Zeile der Betrag des Diagonalelementes größer ist als die Summe der Beträge aller anderen Einträge in derselben Zeile.

Schließlich nennen wir eine *quadratische* Matrix $Q$ **unitär** (im reellen Fall **orthogonal**), falls $Q^HQ = I$ gilt. Permutationsmatrizen sind orthogonal. Weiterhin bilden die Spalten einer unitären Matrix eine orthonormale Menge an Vektoren bezüglich des Skalproduktes $y^Hx$. Schließlich ist die euklidische Norm unter unitären Transformationen *invariant*, d.h. es gilt $\lVert Qx \rVert_2 = \lVert x \rVert_2$ für jedes $x$ und jede unitäre Matrix $Q$.

## QR-Zerlegung

Aus der Tatsache, daß sich jede Menge unabhängiger Vektoren orthonormalisieren läßt, können wir die **QR-Zerlegung** einer Matrix $A$ ableiten. Diese existiert in verschiedenen Varianten:

Im einfachsten Fall ist $A$ invertierbar. Dann läßt sich $A$ als $A = QR$ schreiben, wobei $Q$ eine unitäre Matrix und $R$ eine reguläre obere Dreiecksmatrix ist. Dabei bilden die Spalten von $Q$ eine Orthonormalbasis und die ersten $i$ Spalten spannen jeweils denselben Raum auf wie die ersten $i$ Spalten von $A$. Die Einträge $r_{ij}$ von $R$ sind definiert durch $r_{ij} = \langle A_{.j}, Q_{.i}\rangle$.

Ist $A$ nicht quadratisch, hat jedoch vollen Spaltenrang, so können wir eine nicht-quadratische Matrix $Q$ gewinnen, deren Spaltenvektoren orthonormal sind und die den Spaltenraum von $A$ aufspannen, und entsprechend wie oben eine obere Dreiecksmatrix $R$. Es gilt zwar $Q^HQ = I$, aber da $Q$ nicht quadratisch ist, ist $Q$ nicht unitär.[^2] Alternativ kann man die Spalten von $Q$ mit weiteren linear unabhängigen Spalten zu einer unitären Matrix ergänzen -- dabei muß man allerdings unten in die Matrix $R$ noch entsprechende Nullzeilen einfügen.

Im allgemeinen Fall -- wenn also $A$ nicht notwendigerweise vollen Spaltenrang hat -- ist es unter Umständen erst nötig, die Spalten von $A$ so zu permutieren, daß die linear unabhängigen Spalten zuerst auftreten. Somit gewinnt man mit einer Permutationsmatrix $P$ eine Zerlegung $AP = QR$, wobei $Q$ unitär ist und $R$ "Trapezform" hat: $R$ ist zunächst wieder mit Nullzeilen aufgefüllt und die letzten Spalten von $R$ sind die Koeffizienten der linear abhängigen Spaltenvektoren von $A$ bezüglich der in $Q$ ausgedrückten Orthonormalbasis.

[^2]: Insbesondere ist z.B. die euklidische Norm zwar unter $Q$, nicht aber unter $Q^H$ invariant.

## Lernziele {-}

### Definitionen und Sätze {-}
- Normen: $p$-Normen, $L^p$-Normen, Maximumnnorm (auch auf $C[a,b]$)
- Normenäquivalenz
- Skalarprodukt und induzierte Norm, Standardskalarprodukte auf $\mathbb{C}^n$ und $C[a,b]$
- Cauchy-Schwarz'sche Ungleichung
- Orthogonalität und orthogonales Komplement, orthonormale Menge von Vektoren
- Beschränkte lineare Abbildung
- Norm einer linearen Abbildung, Submultiplikativität, Verträglichkeit, zugeordnete Norm
- Matrixnormen: Zeilensummen-, Spaltensummen-, Spektral- und Frobeniusnorm
- Störungslemma
- Spezielle Matrizen:
    - Diagonalmatrix
    - (elementare) Dreiecksmatrix
    - Permutationsmatrix, Vertauschungsmatrix
    - Hermite'sche Matrix
    - positiv (semi-)definite Matrix
    - diagonal dominante Matrix
    - unitäre/orthogonale Matrix
- Multiplikation Vertauschungsmatrix und elementare untere Dreiecksmatrix
- Unitäre Abbildungen sind abstandserhaltend

### Verfahren {-}
- Gram-Schmidt-Verfahren (+ Modifikation)
- QR-Zerlegung (invertierbar, voller Spaltenrang und allgemein)

### Beweise {-}
- Störungslemma: Teleskopsumme, geometrische Reihe
- Korrektheit Gram-Schmidt-Verfahren

### Übungen {-}
- 2.1.4: Äquivalenz von Maximum- und Summennorm
- 2.4.7: Normeigenschaften der Norm einer beschränkten Abbildung
- 2.4.18-2.4.25: Aufgaben zu Matrixnormen
- 2.5.12-2.5.18: Aufgaben zu speziellen Matrizen
- 2.6.5: Unitäre Invarianz
- Studientag
    - 4a: Beschränktheit von Operatoren und Bestimmung von Normen
    - 4b: Beweis, daß Maximumnorm Grenzwert der p-Normen ist
- EA 2: Gram-Schmidt-Verfahren mit spezieller Norm
- EA 3: Äquivalenz von Summen-, euklidischer und Maximumnorm.
- EA 4: Matrixnormeigenschaften nachweisen