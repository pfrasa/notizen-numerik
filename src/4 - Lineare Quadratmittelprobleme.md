---
previous_sections: 3
---

# Lineare Quadratmittelprobleme

In diesem Kapitel beschäftigen wir uns mit überbestimmten Gleichungssystemen, d.h. Systeme mit mehr Gleichungen als Unbekannte. In der Regel läßt sich in so einem Fall die Aufgabe $Ax = b$ nicht lösen. Stattdessen wollen wir ein $x$ finden, für das $\lVert Ax - b\rVert_2$, das sogenannte **Residuum**, minimal wird. Dies bezeichnen wir als **lineares Quadratmittelproblem** und schreiben auch $Ax \cong b$.

## Zusammenfassung {-}

## Normalgleichungen

Die erste Lösungsmethode für das lineare Quadratmittelproblem besteht darin, das System

$$
A^HAx = A^Hb,
$$
die sogenannten **Normalgleichungen** aufzustellen. Es zeigt sich, daß $x$ genau dann eine Lösung der Normalgleichungen ist, wenn $Ax \cong b$ gilt.

Weiterhin zeigt man, daß $\mathrm{Bild}(A^H)=\mathrm{Bild}(A^HA)$ und $\mathrm{Kern}(A)=\mathrm{Kern}(A^HA)$ gilt. Damit folgt erstens, daß eine Lösung der Normalgleichungen stets existiert, zweitens, daß bei Kenntnis einer Lösung $x$ alle weiteren Lösungen durch Addition von Vektoren aus $\mathrm{Kern}(A)$ entstehen, und drittens, daß die Lösung eindeutig ist, falls $A$ Vollrang besitzt.

Die Matrix $A^HA$ ist Hermite'sch und genau dann positiv definit, wenn $A$ Vollrang hat. In diesem Fall läßt sich im Prinzip das Cholesky-Verfahren für die Lösung der Gleichungen verwenden. In der Praxis ist diese Lösungsmethode jedoch oft eher schlecht konditioniert.

## Lösung durch QR-Zerlegung

Für eine zweite Lösungsmethode berufen wir uns auf die Zerlegung $AP = QR$, wobei $Q$ eine unitäre Matrix ist. Man zeigt dann, daß

$$
\lVert Ax - b\rVert_2 = \lVert R\widetilde{x} - Q^Hb\rVert_2
$$
mit $\widetilde{x} = P^Hx$ gilt.

Teilt man nun die Matrix $R$ und die Vektoren $\widetilde{x}$ und $Q^Hb$ wie folgt in Blöcke auf:
$$
\begin{pmatrix}R_0 & R_1\\0 & 0\end{pmatrix}\begin{pmatrix}x_0\\x_1\end{pmatrix} - \begin{pmatrix}b_0\\b_1\end{pmatrix},
$$
wobei $R_0$ eine obere Dreiecksmatrix ist, so gilt
$$
\lVert Ax-b\rVert_2^2 = \lVert R_0x_0+R_1x_1-b_0\rVert_2^2 + \lVert b_1\rVert_2^2.
$$
Nun ist $b_1$ fest, den anderen Term können wir allerdings gleich $0$ setzen. Dafür wählen wir $x_1$ beliebig und lösen $R_0x_0 = b_0-R_1x_1$ nach $x_0$ auf, was immer gelingt, da $R_0$ invertierbar ist. Wir erhalten dann das Residuum $\lVert b_1\rVert_2$.

Hat $A$ nicht vollen Spaltenrang, so brauchen wir im Übrigen nicht die vollständige Matrix $Q$, sondern können uns mit der Untermatrix $Q_1$ begnügen, deren Spaltenvektoren den Spaltenraum von $A$ aufspannen. Dies ist möglich, da für die Berechnung von $R_0, R_1$ und $b_0$ nur $Q_1$ erforderlich ist und $\lVert b_1 \rVert_2^2 = \lVert b \rVert_2^2 - \lVert b_0 \rVert_2^2$ gilt.

## Householder-Verfahren
Das Gram-Schmidt-Verfahren ist numerisch nicht besonders stabil. Deswegen wird in der Praxis die QR-Zerlegung einer Matrix auf anderem Wege bestimmt, nämlich mit dem sogenannten **Householder-Verfahren**.

Die Idee des Verfahrens besteht darin, mittels Multiplikation mit geeigneten Matrizen die Einträge unterhalb der Diagonalen ähnlich wie im Gauß'schen Eliminationsverfahren zu eliminieren. Anders als beim Gauß-Algorithmus werden hierbei allerdings nicht Dreiecksmatrizen, sondern unitäre Matrizen, die sogenannten **Householder-Matrizen** verwendet. Da das Produkt unitärer Matrizen wieder unitär ist, erhalten wir so eine Zerlegung $H_1\ldots H_{n-1}A = Q^HA = R$ mit einer unitären Matrix $Q$.

Eine Householder-Matrix $H_v$ für einen Vektor $v \neq 0$ ist definiert als
$$
H_v := I - \frac{2}{v^Hv}vv^H
$$
Man zeigt, daß $H_v$ Hermite'sch und unitär ist, daß $H_vv = -v$ gilt und daß $H_vw = w$ für alle zu $v$ orthogonalen Vektoren $w$ ist. Somit repräsentiert $H_v$ eine Spiegelung an einer Hyperebene mit Normalenvektor $v$.

Wir wollen nun herausfinden, mit welcher Householder-Matrix eine Matrix $A$ multipliziert werden muß, damit die erste Spalte $a_{.1}$ von $A$ auf ein Vielfaches von $e^1$ abgebildet wird. Definieren wir

$$
\gamma = -\lVert a_{.1}\rVert_2\begin{cases}a_{11}/|a_{11}| & \text{falls $a_{11} \neq 0$}\\ 1 & \text{sonst}\end{cases}
$$
und
$$
v = a_{.1}-\gamma e^1,
$$
so zeigt sich, daß $H_v a_{.1} = \gamma e^1$ gilt. Haben wir nun in der ersten Spalte die Einträge unterhalb der Diagonalen eliminiert, so verfahren wir mit der verbliebenen $(n-1)\times (n-1)$-Untermatrix auf dieselbe Art weiter usw.

In der Praxis berechnen wir natürlich in der Regel die Householder-Matrizen und deren Produkt nicht explizit, sondern führen alle Eliminationsschritte direkt auf dem Speicherplatz von $A$ und $b$ durch.

Das Householder-Verfahren hat wie der Gauß-Algorithmus Komplexität $O(n^3)$, ist aber von der Konstanten her etwa doppelt so langsam.

## Lernziele {-}

### Definitionen und Sätze {-}
- Lineares Quadratmittelproblem, Residuum
- Normalgleichungen und Äquivalenz zum linearen Quadratmittelproblem
- Existenz und ggf. Eindeutigkeit einer Lösung der Normalgleichungen
- Householder-Matrix und Eigenschaften

### Verfahren {-}
- Lösung über Normalgleichungen
- Lösung über QR-Zerlegung
- Householder-Verfahren

### Beweise {-}
- Äquivalenz zu Normalgleichungen (auch: Aufgabe 4.1.7)
- Lösbarkeit Normalgleichungen:
    - über Kern und Rang (Aufgabe 4.1.2)
- Herleitung Lösung über QR-Verfahren:
    - Durch Multiplikation des Residuums mit $Q^H$ (Invarianz)
- Eigenschaften der Householder-Matrix
- Satz 4.3.5: Korrektheit Householder-Verfahren

### Übungen {-}
- EA 1: Produkte von Q/R-Matrizen
- EA 2: Householder-Verfahren
- Studientag:
    - Aufgabe 9: Anwendung Householder-Verfahren