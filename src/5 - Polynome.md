---
previous_sections: 4
---

# Polynome

In diesem Kapitel betrachten wir Polynome eingehender, da diese in der Numerik an vielen Stellen eine wichtige Rolle spielen (z.B. auch im folgenden Kapitel über Polynominterpolation).

## Zusammenfassung {-}

## Polynome und Monome
### Struktureigenschaften

Zunächst erinnern wir daran, daß sowohl die Menge $\Pi$ aller Polynome als auch die Menge $\Pi_n$ aller Polynome vom Höchstgrad $n$ jeweils auf natürliche Art einen Vektorraum bilden. Durch die Polynome $1, x, x^2, \ldots$ bzw. $1, x, x^2,\ldots x^n$ ist jeweils eine Basis, die **Monombasis** gegeben.

Andererseits bilden Polynome mittels Addition und Multiplikation auch einen Ring. Wir erinnern an den **Fundamentalsatz der Algebra**: Jedes Polynom vom Grad $n \geq 0$ über $\mathbb{C}$ hat genau $n$ Nullstellen. Daraus folgt auch direkt, daß ein Polynom vom Höchstgrad $n$ durch die Werte in $n+1$ verschiedenen Stellen eindeutig definiert ist.[^1]

[^1]: Dies wird im Kapitel über Polynominterpolation noch von Nutzen sein.

Obwohl man Polynome im Allgemeinen nicht dividieren kann, kann man eine **Division mit Rest** durchführen. Sind dabei $p, q$ Polynome mit $\mathrm{deg}(q) \leq \mathrm{deg}(p)$, so existieren eindeutig bestimmte Polynome $s$ und $r$, so daß
$$
p = sq + r,\quad\quad \mathrm{deg}(r) < \mathrm{deg}(q)
$$
gilt. Dies führt auch gleich zum **Euklidischen Algorithmus** und dem Begriff des **größten gemeinsamen Teilers** zweier Polynome

### Das Horner-Verfahren

Mit dem **Horner-Verfahren** oder -Schema lernen wir einen Algorithmus kennen, der gleich für mehrere Zwecke eingesetzt werden kann und später auch in Abwandlungen von Nutzen sein wird.

Die Idee besteht darin, ein Polynom in Monomdarstellung $p(x) = a_0 + a_1x + \ldots + a_nx^n$ darzustellen als
$$
p(x) = a_0 + x(a_1 + x(a_2 + \ldots + xa_n)\ldots)
$$
und das Polynom an einer gegebenen Stelle dann von innen nach außen auszuwerten.

Neben der Berechnung des Polynoms an einer Stelle, kann das Verfahren jedoch auch benutzt werden, um mit Rest zu dividieren bzw. Linearfaktoren abzuspalten. Spalten wir mehrere Linearfaktoren auf einmal ab, so läßt sich dies im **erweiterten Hornerschema** bewerkstelligen.

Schließlich können wir im **vollständigen Hornerschema** ein Polynom $n$ mal durch einen Faktor $x-\alpha$ teilen und erhalten so eine Darstellung
$$
p(x) = a^\prime_0 + a_1^\prime(x-a)+\ldots + a_n^\prime(x-a)^n
$$
Durch Koeffizientenvergleich mit der Taylor-Entwicklung von $p$ können wir so die $i$-te Ableitung von $p$ an der Stelle $\alpha$ durch
$$
p^{(i)}(\alpha) = i!a_i^\prime
$$
auswerten.

## Tschebyscheff-Polynome

Durch die dreigliedrige Rekursion
$$
T_0 = 1,\quad T_1 = x,\quad T_n = 2xT_{n-1}-T_{n-2}
$$
wird eine Basis von Polynomen definiert, die sogenannten **Tschebyscheff-Polynome erster Art**.

Auf dem Intervall $[-1,1]$ werden die Polynome auch durch $T_n(x) = \cos(n\arccos x)$ definiert und können dann eindeutig forgesetzt werden. Es folgt sofort, daß die Nullstellen von $T_n$ alle einfach sind, im Intervall $(-1,1)$ liegen und durch
$$
x_k = \cos\left(\frac{(2k-1)\pi}{2n}\right),\quad k=1,\ldots,n
$$
gegeben sind.

Durch die Rekursion
$$
U_0 = 1,\quad U_1 = 2x,\quad U_n = 2xU_{n-1}-U_{n-2}
$$
sind die sogenannten **Tschebyscheff-Poynome zweiter Art** gegeben. Es besteht die Beziehung $T_n^\prime = nU_{n-1}$.

## Orthogonale Polynome

Sei $I = (a, b)$ ein offenes Intervall auf $\mathbb{R}$. Eine stets positive Funktion $\rho$ auf $(a,b)$, für die $\int_a^b q\rho$ für alle Polynome $q$ existiert, nennen wir **Gewichtsfunktion**.

Eine solche Funktion induziert gemäß
$$
\langle v, w\rangle_\rho := \int_a^b vw\rho
$$
ein Skalarprodukt auf $\Pi$ und damit auch eine Norm $\lVert .\rVert_\rho$.

Ist nun eine Folge $(u_i)_i$ von Polynomen gegeben, sodaß stets $u_i \in \Pi_i$ gilt und all diese Polynome paarweise orthogonal bzgl. dieses Skalarproduktes sind,[^2] so bezeichnen wir diese Polynome als bezüglich $\rho$ **orthogonale Polynome**. Aufgrund des Verfahres von Gram-Schmidt ist garantiert, daß so eine Folge stets existiert. Ferner sind diese Polynome bis auf multiplikative Faktoren eindeutig bestimmt; zur eindeutigen Festlegung kann man bspw. Verlangen, daß die Polynome normiert sind und der Leitkoeffizient positiv ist, oder daß sie einen bestimmten Wert an einer bestimmten Stelle annehmen.

[^2]: Äquivalent fordert man jeweils die Orthogonalität von $u_i$ zu allen Polynomen aus $\Pi_{i-1}$.

Die Tschebyscheff-Polynome erster Art sind orthogonal bezüglich der Gewichtsfunktion
$$
\rho(x) = \frac{1}{\sqrt{1-x^2}}
$$
auf $(-1,1)$ und erfüllen zusätzlich jeweils die Bedigung $T_i(1) = 1$.

Wir suchen nun Polynome $P_i$, die zu der Gewichtsfunktion $\rho(x) = 1$ auf $(-1,1)$ orthogonal sind und zusätzlich $P_i(1)=1$ erfüllen. Die so gewonnenen Polynome nennen wir die **Legendre-Polynome**. Für diese können wir auch die geschlossene **Rodrigues-Formel**
$$
P_i(x)=\frac{1}{2^i i!}\frac{\mathrm{d}^i}{\mathrm{d}x^i} \left(\left(x^2-1\right)^i\right)
$$
angeben.

Im Allgemeinen zeigt man, daß für jede Folge $(u_i)$ orthogonaler Polynome eine dreigliedrige Rekursion der Form
$$
u_0 = \gamma,\quad \beta_1u_1 = (x-\alpha_0)u_0,\quad \beta_{i+1}u_{i+1} = (x-\alpha_i)u_i-\delta_iu_{i-1}
$$
existiert.

Mit dem Horner-Algorithmus konnten wir auf einfache Art und Weise ein Polynom, das in Monomdarstellung gegeben war, an einer bestimmten Stelle $x_0$ auswerten. Ist nun aber das Polynom $p$ dargestellt in Bezug auf eine Orthogonalbasis, d.h. $p = a_0u_0 + a_1u_1 + \ldots + a_nu_n$ und kennen wir die Rekursionsformeln der Basispolynome, so können wir mit dem **Clenshaw-Algorithmus** ebenfalls den Wert $p(x_0)$ berechnen. Dabei lösen wir zunächst die Formel für $u_n$ nach diesem Term um und setzen dies in $p(x_0)$ ein, um $u_n$ zu eliminieren. Nachdem wir die Terme neu gruppiert haben, eliminieren wir $u_{n-1}$ usw. Die Schritte, die dabei durchgeführt werden müssen, sind im Wesentlichen stets die Gleichen. Am Ende erhalten wir wie gewünscht den Wert $p(x_0)$.

Zum Schluß weisen wir noch auf zwei wichtige Eigenschaften orthogonaler Polynome hin: Zum einen sind alle Nullstellen dieser Polynome einfach und liegen im offenen Intervall $I$, auf dem die Gewichtsfunktion definiert ist; dies hatten wir für die Tschebyscheff-Polynome bereits gezeigt. Zum anderen besitzen die Nullstellen die folgende Trennungseigenschaft: Zwischen zwei aufeinanderfolgenden Nullstellen eines Polynoms $u_{i+1}$ liegt jeweils eine Nullstelle von $u_i$. Letzteres zeigen wir allerdings nur für die Tschebyscheff-Polynome.

## Lernziele {-}

### Definitionen und Sätze {-}
- Ring- und Vektorraumeigenschaften von Polynomen, Monombasis
- Fundamentalsatz der Algebra, Eindeutigkeit von Polynomen
- Division mit Rest und euklidischer Algorithmus
- Tschebyscheff-Polynome 1. und 2. Art:
    - Rekursionsformeln
    - geschlossene Form auf $(-1,1)$
    - explizite Monomdarstellung für die ersten paar Polynome
    - Beziehung über Ableitung
    - Gewichtsfunktion
- Legendre-Polynome:
    - Gewichtsfunktion
    - Rodrigues-Formel
    - explizite Monomdarstellung für die ersten paar Polynome
- Orthogonale Polynome
- Existenz einer dreigliedrigen Rekursion
- Lage von Nullstellen und Trennungseigenschaft

### Verfahren {-}

- Horner-Schema:
    - zur Auswertung von $p(\alpha)$, wenn $p$ in Monomdarstellung
    - zur Division mit Rest bzw. Abspalten von Linearfaktoren
    - mehrfaches Abspalten: erweitertes Schema
    - $n$-malige Division: vollständiges Schema => Darstellung bzgl. $(x-\alpha)^i$ => Berechnung von $p^{(i)}(\alpha)$
- Gewinnung von Monomdarstellung von orthogonalen Polynomen:
    - Orthogonalität bzgl. Monomen ausnutzen und Gleichungssystem lösen
    - Oder über Gram-Schmidt
- Clenshaw-Algorithmus:
    - Auswertung von $p(\alpha)$, wenn $p$ bzgl. Orthogonalbasis gegeben
    
### Beweise {-}

- Satz 5.1.6: Lineare Unabhängigkeit der Monome
- Satz 5.1.13: Eindeutigkeit durch Werte an Stützstellen
- Satz 5.2.3: Darstellung Tschebyscheff-Polynome auf $(-1,1)$
- Satz 5.2.5 und 5.2.6: Eigenschaften der Tschebyscheff-Polynome
- Satz 5.3.2: Gewichtsfunktion induziert Skalarprodukt
- Satz 5.3.13(i): Existenz und Eindeutigkeit orthogonaler Polynome (s. auch Aufgabe 5.3.8)
- Satz 5.3.18 (ohne (ii)): Beweis Rodrigues-Formel
- Satz 5.3.26: Nullstellen orthogonaler Polynome

### Aufgaben {-}

- 5.1.21-22: Anwendungen Horner-Schema
- 5.2.2(b) Darstellung eines Polynoms anhand bestimmter Basis
- 5.2.7: Orthogonalität von Tschebyscheff-Polynomen
- 5.3.12+5.3.16: Eigenschaften von orthogonalen Polynomen mit gerader Gewichtsfunktion
- 5.3.25: Anwendung Clenshaw-Algorithmus
- Studientag:
    - 11: Aufgaben zu Horner-Schema
    - 12: Rekursionsformeln und Orthogonalitätseigenschaften von Polynomen bestimmen
- EA 5: Tschebyscheff-Polynome: Andere Darstellung
- EA 7: Ableitungen Legendre-Polynome