---
previous_sections: 6
---

# Quadratur

In diesem Kapitel geht es um **numerische Integration**, auch *Quadratur* genannt.

## Zusammenfassung {-}

## Interpolatorische Quadraturformeln
Wir können das Integral einer Funktion $f$ über einem Interval $[a,b]$ dadurch approximieren, daß wir zunächst $[a,b]$ in mehrere Teilintervalle aufteilen und dann auf jedem Teilintervall eine approximierende Funktion integrieren. Bei **interpolatorischen Quadraturformeln** sind diese Funktionen jeweils Polynome, die $f$ an bestimmten Stützstellen interpolieren.

### Elementare Quadraturformeln
Ist ein Teilintervall $[\alpha,\beta]$ gegeben, so können wir $n+1$ Stützstellen $\eta_0, \ldots, \eta_n$ festlegen. Das Interpolationspolynom zu diesen Stützstellen ist dann in Lagrange'scher Darstellung
$$
p(x)= \sum_{j=0}^n L_j(x)f(\eta_j).
$$

Dadurch gewinnen wir eine Näherung des Integrals in diesem Intervall:
$$
\int_\alpha^\beta f(x)\mathrm{d}x \approx \int_\alpha^\beta p(x)\mathrm{d}x = (\beta-\alpha)\sum_{j=0}^n \gamma_j f(\eta_j) =: Q_\alpha^\beta[f]
$$
mit Gewichten $\gamma_j$ die sich aus $\gamma_j = \frac{1}{\beta-\alpha}\int_\alpha^\beta L_j(x)\mathrm{d}x$ ergeben.[^1] Eine Formel dieser Art nennen wir **elementare Quadraturformel**.

[^1]: Dadurch sind die Gewichte so normiert, daß $\sum_{j=0}^n \gamma_j = 1$. Man könnte aber auch jeweils die Gewichte selber bereits mit $\beta-\alpha$ multiplizieren, dadurch werden sie aber in der Regel vom genauen Teilintervall abhängig.

Einige wichtige elementare Quadraturformeln sind:

- Die **linksseitige Rechteckregel** wählt alleine den linken Randpunkt, d.h. $\eta_0 = \alpha$ als einzige Stützstelle. Man erhält die Formel $Q_\alpha^\beta[f] = (\beta-\alpha)f(\alpha)$. Analog wählt die **rechtsseitige Rechteckregel** $\eta_0 = \beta$, was zu $Q_\alpha^\beta[f] = (\beta-\alpha)f(\beta)$ führt.
- Die **Mittelpunktregel** wählt als einzige Stützstelle den Mittelpunkt, $\eta_0 = \frac{\alpha+\beta}{2}$. Wir erhalten $Q_\alpha^\beta[f] = (\beta-\alpha)f\left(\frac{\alpha+\beta}{2}\right)$.
- Die **Trapezregel** wählt beide Intervallenden als Stützstellen. Wir erhalten $Q_\alpha^\beta[f] = \frac{\beta-\alpha}{2}(f(\alpha)+f(\beta))$.
- Die **Simpsonregel** wählt die Stützstellen $\eta_0 = \alpha, \eta_1 = \frac{\alpha+\beta}{2}, \eta_2 = \beta$. Wir erhalten $Q_\alpha^\beta[f] = \frac{\beta-\alpha}{6}(f(\alpha)+4f\left(\frac{\alpha+\beta}{2}\right)+f(\beta))$.

### Summierte Quadraturformeln
Summierte Quadraturformeln entstehen nun dadurch, daß wir das Ausgangsintervall $[a,b]$ in Teilintervalle aufteilen und auf jedem dieselbe Quadratuformel anwenden. Dabei sind die Gewichte $\gamma_i$ vom genauen Teilintervall unabhängig. Wir wollen uns auf den Fall gleich großer Teilintervalle beschränken. Wählen wir $m$ Teilintervalle $[a_i,b_i]$ der Breite $h := \frac{b-a}{m}$, so erhalten wir die Formel
$$
Q_{a,m}^b[f] := h\sum_{i=1}^m\sum_{j=0}^n \gamma_jf(\chi_i(\eta_j))
$$
mit Funktionen $\chi_i$, die $[\alpha,\beta]$ bijektiv auf $[a_i,b_i]$ abbilden.

## Quadraturfehler
Wir interessieren uns nun für den Approximationsfehler unseres Quadraturverfahrens. Diese Überlegungen können wir auf die Erkenntnisse zum Interpolationsfehler zurückführen. Haben wir ein System mit $n+1$ Stützstellen, so gilt für den Quadraturfehler einer elementaren Quadraturformel
$$
\left|\int_\alpha^\beta f - Q_\alpha^\beta[f]\right| \leq C(\beta-\alpha)^{n+2}\lVert f^{(n+1)} \rVert_{[\alpha,\beta]}
$$
für eine Konstante $C$, die nicht vom Intervall $\beta-\alpha$ abhängt.

Ist $n$ gerade und alle Stützstellen sind symmetrisch um den Mittelpunkt des Intervalls verteilt, der selber auch Stützstelle ist, so können wir mittels Hermite-Interpolation ein Polynom $\widetilde{p}$ vom Grad $n+1$ finden, für das $\int_\alpha^\beta p = \int_\alpha^\beta \widetilde{p}$ gilt. Somit gilt dann
$$
\left|\int_\alpha^\beta f-Q_\alpha^\beta[f]\right| \leq C(\beta-\alpha)^{n+3}\lVert f^{(n+2)} \rVert_{[\alpha,\beta]}.
$$

Unter diesen Voraussetzungen sehen wir, daß die Simpson-Regel (für kleine Intervalle) unter unseren bisherigen Regeln die beste Approximation liefert, da der Fehler proportional zu $(\beta-\alpha)^5$ ist. Erstaunlicherweise sind aber die Fehler der Mittelpunktregel und der Trapezregel beide proportional zu $(\beta-\alpha)^3$, obwohl die Mittelpunktregel eine Stützstelle weniger hat. Tatsächlich ist bei der Mittelpunktregel sogar die Konstante besser.

Für die summierten Formeln haben wir entsprechend die Abschätzungen
$$
\left|\int_a^b f - Q_{a,m}^b[f]\right| \leq C(b-a)h^{n+1}\lVert f^{(n+1)}\rVert_{[a,b]}
$$
bzw. für symmetrisch verteilte Nullstellen
$$
\left|\int_a^b f - Q_{a,m}^b[f]\right| \leq C(b-a)h^{n+2}\lVert f^{(n+2)}\rVert_{[a,b]}.
$$

### Ordnung und Exaktheit
Wir sagen, daß eine *elementare* Quadraturformel **Exaktheitsgrad** $r$ hat, wenn sie alle Polynome vom Höchstgrad $r$ exakt integriert und es ein Polynom vom Grad $r+1$ gibt, das nicht exakt integriert wird. Offensichtlich ist der Exaktheitsgrad einer interpolatorischen Quadraturformel mit $n+1$ Stützstellen mindestens $n$.

Eine *summierte* Quadraturfomel hat **Ordnung** $p$, wenn es eine Konstante $C$ gibt, sodaß
$$
\left|\int_a^b f - Q_{a,m}^b[f]\right| \leq C(b-a)h^p\lVert f^{(p)}\rVert_{[a,b]}
$$
für alle hinreichend glatten $f$ gilt.

Hat eine elementare Quadraturformel den Exaktheitsgrad $r$, so gilt, daß die entsprechende summierte Quadraturformel die Ordnung $r+1$ besitzt.

## Gauß-Legendre-Quadratur
Man kann sich nun die Frage stellen, ob man die Stützstellen einer elementaren Formel so wählen kann, daß wir einen möglichst hohen Exaktheitsgrad erreichen. Es stellt sich heraus, daß bei $n+1$ Stützstellen der Exaktheitsgrad maximal $2n+1$ sein kann. Um nun die Stützstellen zu optimieren, könnte man ein Gleichungssystem mit den Stützstellen und Gewichten als Unbekannte aufstellen und die Exaktheit für die Monombasis fordern. Wir wollen die Frage allerdings systematischer beantworten und beschränken uns dabei auf das Intervall $[-1,1]$.

Es gilt nämlich, daß eine Quadraturformel mit $n+1$ Stützstellen genau dann einen Exaktheitsgrad von mindestens $n+1+s$ hat, wenn das zu den Stützstellen zugehörige Knotenpolynom $\omega_{n+1}$ die Bedingung $\int_{-1}^1 \omega_{n+1}q = 0$ für alle $p\in\Pi_s$ erfüllt. Ist nun aber $s=n$, so ist dies genau die definierende Bedingung für das Legendre-Polynom $P_{n+1}$. Wir erreichen also den maximalen Exaktheitsgrad genau dann, wenn wir als Stützstellen die Nullstellen des Legendre-Polynoms wählen. Dieses Verfahren nennt man **Gauß-Legendre-Quadratur**. Davon ausgehend kann man die Gewichte dieser Formeln explizit angeben und den Quadraturfehler abschätzen, was allerdings zu komplizierten Formeln führt.

## Romberg-Verfahren

Das **Romberg-Verfahren** ist eine Quadratur-Technik, bei der der Fehler der summierten Trapezregel durch mehrfache Anwendung signifikant reduziert wird.

Es handelt sich dabei um eine spezifische Anwendung der in der Numerik in vielen Kontexten nützlichen sogenannten **Richardson-Extrapolation**. Bei dieser können wir unter günstigen Bedingungen mehrere Näherungen mit einem Fehler einer bestimmten Ordnung (in Abhängigkeit eines Parameters $h$ mit $h \to 0$) so miteinander kombinieren, dass der resultierende Fehler von höherer Ordnung ist.

Spezieller gehen wir davon aus, dass es ein Polynom $p(x) = \mathcal{T} + a_1x + a_2x^2 + \ldots + a_nx^n$ gibt, wobei $\mathcal{T}$ der gesuchte Wert ist, und dass sich die Näherung $T(h)$ darstellen lässt als $T(h) = p(h^k) + R(h)$ für ein festes $k$, wobei $R(h)$ proportional zu $h^{(n+1)k}$ ist. Nun ist uns $p$ bzw. dessen Koeffizienten in aller Regel unbekannt. Würden wir $p$ allerdings kennen, so könnten wir den gesuchten Wert ganz einfach durch $p(0) = \mathcal{T}$ bestimmen. Stattdessen werden wir ein Polynom $q$ konstruieren, das $p$ annähern soll, und bestimmen dann $q(0) \approx \mathcal{T}$.

Das Polynom $q$ konstruieren wir mit den Interpolationsverfahren aus Kapitel 6. Wir wählen $n+1$ Stützstellen $h_0 > \ldots > h_n$ und wählen jeweils die Werte $q(h_i) = T(h_i)$, d.h. wir vernachlässigen bei jeder Stützstelle den Fehler $R(h_i)$. Damit ist offensichtlich an jeder Stützstelle der Fehler gegenüber $p$ proportional zu $h_i^{(n+1)k}$. Man sieht aber auch leicht, dass der Näherungsfehler $R(0) = q(0) - p(0) = q(0) - \mathcal{T}$ dann proportional zu $h_0^{(n+1)k}$ ist. Da wir $q$ nur an einer Stelle auswerten müssen, können wir den Algorithmus von Aitken und Neville anwenden. Dabei müssen wir die Näherungen jeweils in den $n+1$ Stützstellen berechnen.

Ein Beispiel für die Richardson-Extrapolation (der nichts mit Quadratur zu tun hat) ergibt sich aus der Fehlerabschätzung für den Differenzenquotienten als Näherung für die Ableitung, wie in Kapitel 1 besprochen. Für festes $x$ definieren wir $T(h) := \frac{f(x+h)-f(x)}{h}$. Dann ist aufgrund des Satzes von Taylor (unter passenden Bedingungen an stetige Differenzierbarkeit) für beliebiges $n$:

$$
T(h) = f^\prime(x) + \frac{f^{\prime\prime}(x)}{2}h + \ldots + \frac{f^{(n+1)}(x)}{(n+1)!}h^n + R(h)
$$

und $R(h)$ ist proportional zu $h^{n+1}$. Somit können wir das Richardson-Verfahren (mit $k=1$) anwenden.

Wir betrachten nun das Romberg-Verfahren. Man kann zeigen, dass für jedes $n$ ein Polynom $p$ mit Ordnung $\leq n$ existiert, so dass für die summierte Trapezregel gilt:

$$
T_{a,m}^b[f] = p(h^2) + R(h),\quad h = \frac{b-a}{m},\quad p(0) = I_a^b[f],\quad R(h) \propto h^{2(n+1)}
$$

Hier können wir die Richardson-Extrapolation anwenden, indem wir $m_0 < \ldots < m_n$ wählen und dann jeweils die summierte Trapezregel mit $m_i$ Intervallen der Breite $h_i := \frac{b-a}{m_i}$ anwenden. Damit erhält man ein Verfahren, bei dem selbst mit vergleichsweise wenigen Funktionsauswertungen eine ziemlich genaue Näherung bestimmt werden kann.

## Lernziele {-}

### Definitionen {-}
- Elementare / interpolatorische Quadraturformel, summierte Formel
- Ordnung, Exaktheit, Zusammenhang
- Maximaler Exaktheitsgrad
- Nullstellen des Legendre-Polynoms sind optimale Stützstellen
- asymptotische Fehlerentwicklung Trapezregel (für Romberg-Verfahren)

### Verfahren {-}

- Bestimmung einer interpolatorischen Quadraturformel, insb. deren Gewichte
- Elementare und summierte Quadraturformeln:
    - Trapezregel
    - Mittelpunktregel
    - Simpson-Regel
    - inkl. Fehlerabschätzungen / Exaktheitsgraden / Ordnungen
- Gauß-Legendre-Quadratur (Gewichte / Abschätzung?)
- Richardson-Extrapolation
- Romberg-Verfahren

### Beweise {-}

- Fehlerabschätzungen Quadraturformeln:
    - Allgemeiner Fall: Anwendung Interpolationsfehler
    - Symmetrische Stützstellen: Hermite-Interpolation (s. auch Aufgabe 7.2.17)
    - Summierte Formeln
- Bemerkung 7.2.16: Unabhängigkeit der Fehlerkonstanten vom Intervall
- Satz 7.3.3: Exaktheitsgrad über Orthogonalität
- Satz 7.3.5: Quadraturfehler Gauß-Legendre
- Fehlerabschätzung Richardson-Extrapolation

### Übungen {-}

- 7.2.5: Konstruktion einer Quadraturformel zu gegebenen Stützstellen
- 7.2.26: Exaktheitsgrade für Quadraturformeln bestimmen
- Studientag:
    - 16: Quadraturformeln, Gewichte, Substitutionen
- EA 6.1: Finden von optimalen Stützstellen:
    - "elementarer Ansatz" durch Gleichungssystem, s. auch Beispiel 7.3.1
    - Konstruktion über Satz 7.3.3 (Orthogonalität des Knotenpolynoms)
    - Achtung: Normierungsfaktor der Gewichte fehlt in Aufgabe
- EA 6.2:
    - Verallgemeinerung der Quadratur (andere Arten von Integralen)
    - Anwendung von Satz 6.6.1
- EA 6.5:
    - Anwendung summierte Trapezregel
    - Anwendung Romberg-Verfahren
