---
previous_sections: 2
---

# Lineare Gleichungssysteme

In diesem Kapitel wird das Lösen linearer Gleichungssysteme von numerischer Perspektive betrachtet.

## Zusammenfassung {-}

## Kondition linearer Gleichungssysteme

Bei linearen Gleichungssystemen haben wir es mit vektor- oder matrixwertigen Eingabedaten zu tun, deswegen müssen wir für diesen Fall den Begriff des relativen Fehlers leicht anpassen:

$$
\varepsilon_\alpha = \frac{\widetilde{\alpha}-\alpha}{\lVert \alpha \rVert}
$$
Der relative Fehler bezieht sich also immer auf eine bestimmte Norm.

Wir fragen uns zunächst, wie sich Fehler im Eingabevektor $b$ auf die Berechnung der eindeutigen Lösung $x$ von $Ax = b$ mit invertierbarem $A$ auswirken. Dafür müssen wir zunächst eine Matrixnorm $\lVert . \rVert_\ast$ wählen. Es stellt sich heraus, daß die Beziehung

$$
\lVert \varepsilon_x \rVert \leq \mathrm{cond}_\ast(A) \lVert \varepsilon_b \rVert
$$

gilt, wobei $\mathrm{cond}_\ast(A) := \lVert A\rVert_\ast \lVert A^{-1} \rVert_\ast$ die **Konditionszahl der Matrix** bezüglich der gewählten Norm ist.

Wir untersuchen dann weiter den Fall, daß sowohl $A$ als auch $b$ gestört sind. Hierfür sei als Matrixnorm die der Vektornorm zugeordnete gewählt. Ist dann $\lVert \Delta_A \rVert \lVert A^{-1} \rVert < 1$, dann gilt

$$
\lVert \varepsilon_x \rVert \leq \frac{\mathrm{cond}(A)}{1-\lVert \Delta_A \rVert\lVert A^{-1}\rVert}(\lVert\varepsilon_A\rVert + \lVert\varepsilon_b\rVert).
$$

## Lösungsverfahren

Viele Verfahren für das Lösen linearer Gleichungen $Ax = b$ bestimmen zunächst (implizit oder explizit) eine Faktorisierung $A=ST$ und gehen dann wie folgt vor:

- Löse $Sy = b$ nach $y$ auf
- Löse $Tx = y$ nach $x$ auf

Dabei sollten die neuen Gleichungssysteme einfacher zu lösen sein.

Ist eine der Matrizen eine Dreiecksmatrix, so läßt sich das Gleichungssystem sehr einfach mittels **Vorwärtsersetzung** (bei unteren Dreiecksmatrizen) oder **Rückwärtsersetzung** (bei oberen Dreiecksmatrizen) lösen. Hierbei kann man jeweils zeilen- oder spaltenorientiert vorgehen. Diese Verfahren benötigen $O(n^2)$ Rechenoperationen und können ggf. parallelisiert werden.

## Gaußelimination und LR-Faktorisierung

Beim Gauß'schen Eliminationsverfahren wird eine invertierbare Matrix $A$ (unter Veränderung der rechten Seite $b$) in eine untere Dreiecksmatrix transformiert und das Gleichungssystem danach durch Rückwärtsersetzung gelöst. Implizit findet hier die sogenannte **LR-Faktorisierung** statt. Dabei setzen wir zunächst $A^{(1)} = A$ und $b^{(1)} = b$ und führen dann für $1 \leq i < n$ jeweils folgende Schritte aus:

1. Finde ein Nichtnullelement unterhalb des $i$-ten Diagonaleintrags von $A^{(i)}$, das **Pivot-Element**
2. Vertausche ggf. die Zeilen so, daß das Pivot-Element auf der Diagonalen liegt: $\widetilde{A^{(i)}} := T^{(i)}A^{(i)}, \widetilde{b^{(i)}} := T^{(i)}b^{(i)}$ für eine Vertauschungsmatrix $T^{(i)}$.
3. Eliminiere die Elemente unterhalb des Pivot-Elementes durch Multiplikation mit einer elementaren unteren Dreiecksmatrix $L^{(i)}$, die die **Eliminationsfaktoren** enthält: $A^{(i+1)} := L^{(i)}\widetilde{A^{(i)}}, b^{(i+1)} := L^{(i)}\widetilde{b^{(i)}}$.

Falls hierbei keine Zeilenvertauschungen stattfanden, so erhalten wir am Ende eine obere Dreiecksmatrix $R := A^{(n)} = L^{(n-1)}\cdots L^{(1)}A$. Die Matrix $L := (L^{(n-1)}\cdots L^{(1)})^{-1}$ ist eine untere Dreiecksmatrix mit normierter Diagonale und wir erhalten somit die Faktorisierung $LR=A$.

Falls Zeilenvertauschungen durchgeführt werden müssen, kann man zeigen, daß diese Vertauschungen "durchgereicht" werden können. Wir erhalten dann die Beziehung $LR = PA$ mit einer Permutationsmatrix $P$.

Bei der tatsächlichen Durchführung der Gaußelimination wird man sich nicht für die Zwischenergebnisse (und oftmals auch nicht für die Matrix $L$) interessieren und daher alle Schritte auf dem Speicherplatz der Eingabedaten $A$ und $b$ durchführen. Man sieht dann, daß für das Verfahren $O(n^3)$ Rechenschritte benötigt werden, was deutlich schlechter ist als die Rückwärtsersetzung.

Der Algorithmus erlaubt eine gewisse Freiheit bei der Wahl von Pivotelementen. Hierbei will man die Elemente so wählen, daß der Algorithmus numerisch möglichst stabil ist. Pivot-Strategien sind unter anderem:

- **einfache Spalten-Pivot-Suche**: Wir wählen jeweils das betragsmäßig größte Element in seiner Spalte
- **Total-Pivot-Suche**: Wir wählen das betragsmäßig größte Element der gesamten Restmatrix als Pivot; hierbei werden auch Zeilen permutiert, was eine Anpassung des Algorithmus nach sich zieht
- **Skalierte Spalten-Pivot-Suche**: Wir wählen das Element in der Spalte, das gegenüber den anderen Einträgen in seiner Zeile am dominantesten ist, d.h. bei gegebener Spalte $j$ wählen wir dasjenige $a_{ij}$ für das $|a_{ij}|/\sum_{k=j}^n |a_{ik}|$ maximal ist.

Die letzten beiden Strategien sind deutlich aufwändiger, sind aber dafür meist numerisch stabiler.

## Cholesky-Verfahren

Eine berechnungsmäßig günstigere Faktorisierung erhalten wir anhand der **Cholesky-Faktorisierung** $A = LL^H$ mit einer unteren Dreiecksmatrix $L$ mit positiven Diagonalelementen. Eine solche Faktorisierung existiert immer genau dann, wenn $A$ Hermite'sch und positiv definit ist. Durch den Faktorisierungsansatz lassen sich die Elemente von $L$ berechnen, z.B. im Falle einer $3\times 3$-Matrix:

$$
LL^H = \begin{pmatrix}|l_{11}|^2 & \ast & \ast\\l_{21}\overline{l_{11}} & |l_{21}|^2 + |l_{22}|^2 & \ast\\ l_{31}\overline{l_{11}} & l_{31}\overline{l_{21}} + l_{32}\overline{l_{22}} & |l_{31}|^2 + |l_{32}|^2 + |l_{33}|^2\end{pmatrix} = A
$$
Man kann nun die einzelnen Elemente ganz einfach zeilen- oder spaltenweise berechnen. Der resultierende Algorithmus ist etwa doppelt so schnell wie der Gauß-Algorithmus (aber immer noch $O(n^3)$).

## Tridiagonale Matrizen

Wir betrachten Gleichungssysteme der Form

$$
e_ix_{i-1}+d_ix_i+f_ix_{i+1}=b_i,\quad i=1,\ldots,n 
$$

wobei $e_0 = f_n = 0$ gilt. Schreibt man diese in Matrix-Vektor-Form, so hat die zugehörige Matrix $\mathrm{tridiag}(e,f,g)$ in jeder Zeile maximal drei Einträge, die nicht $0$ sind. Wendet man darauf den Gauß-Algorithmus an, so ist die Anzahl der Eliminationen pro Zeile konstant. Der resultierende Algorithmus wird auch **Thomas-Algorithmus** genannt und hat folglich die Komplexität $O(n)$.

Bei großen Matrizen kann man versuchen, diesen Algorithmus durch das Verfahren der **zyklischen Reduktion** zu parallelisieren. Dabei verteilen wir jede der Gleichungen mit ungeradem Index $i$ auf einen Thread. Nun können wir jeweils die Gleichung mit Index $i-1$ nach $x_{i-1}$ und die Gleichung mit Index $i+1$ nach $x_{i+1}$ auflösen. Dies setzen wir nun in die Gleichung mit Index $i$ ein und erhalten eine neue Gleichung, in der nur noch $x_{i-2}, x_i$ und $x_{i+2}$ auftreten. Nach Abarbeitung aller Threads haben wir so alle Gleichungen mit geradem Index eliminiert und haben nunmehr ein nur noch halb so großes System. Auf dieses können wir das Verfahren dann rekursiv anwenden, bis das resultierende System klein genug ist, um direkt gelöst werden zu können.

## Lernziele {-}

### Definitionen und Sätze {-}
- Relativer Fehler bei Vektoren / Matrizen
- Konditionszahl einer Matrix
- Fehlerabschätzungen lineare Gleichungssysteme (wenn nur $b$ gestört oder wenn $A$ und $b$ gestört)
- Tridiagonale Matrix

### Verfahren {-}
- Allgemeines Lösungsverfahren durch Faktorisierung
- Vorwärts-/Rückwärtsersetzung (jeweils zeilen- und spaltenorientiert)
- LR-Faktorisierung und Gauß'sche Elimination
- einfache und skalierte Spalten-Pivot-Suche, Total-Pivot-Suche
- Cholesky-Faktorisierung
- Thomas-Algorithmus
- Zyklische Reduktion

### Beweise {-}
- Herleitungen Konditionszahlen:
    - Ausnutzen von Submultiplikativität, Verträglichkeit und Störungslemma
- Existenz der LR-Faktorisierung:
    - Über elementare untere Dreiecksmatrizen
    - Bei Zeilenvertauschungen Anwendung Satz 2.5.11
- Cholesky-Faktorisierung existiert $\iff$ Matrix ist positiv definit (auch: Aufgabe 3.4.4):
    - Nur Wurzeln aus positiven Zahlen
    - Keine Division durch $0$

### Übungen {-}
- 3.1.7: Zur Spektralnorm 
- 3.3.2: Keine Zeilenvertauschungen bei diagonal dominanten Matrizen
- Studientag:
    - 6a: Ausnutzung Polarkoordinaten
    - 6b: Induktive Konstruktion, ähnlich Cholesky-Faktorisierung
- EA 2: Eigenschaften der Konditionszahl
- EA 3: Äquilibrierte Matrix
- EA 7: Abwandlungen Cholesky-Verfahren