# Fehleranalyse und Computerzahlen
In diesem Kapitel untersuchen wir zum einen, wie reelle Zahlen im Computer dargestellt werden, und zum anderen, wie sich Fehler auf ein zu berechnendes Resultat auswirken.

## Zusammenfassung {-}
## Absoluter und relativer Fehler
Sei $\alpha$ ein zu berechnender Wert und $\widetilde{\alpha}$ eine Näherung. Dann ist $\Delta_\alpha = \widetilde{\alpha} - \alpha$ der **absolute Fehler** und $\varepsilon_\alpha = \frac{\widetilde{\alpha}-\alpha}{|\alpha|}$ der **relative Fehler**. Der relative Fehler ist i.d.R. relevanter und meist interessiert man sich auch nur für dessen Betrag.

Wir diskutieren die Begriffe anhand des Beispiels der Volumenberechnung einer Pyramide durch die Formel $V = \frac{1}{3}Fh$. Hierbei können die Eingabedaten $F$ und $h$ fehlerbehaftet sein und wir wollen untersuchen, wie sich diese Fehler auf den Fehler in $V$ auswirken. Wie später oft, betrachten wir den Fehler nur in **linearer Näherung**. Wir stellen fest, daß $\varepsilon_V \approx \varepsilon_h + \varepsilon_F$ gilt.

## Maschinenarithmetik
### Gleitkommazahlen
Im Computer kann man nicht überabzählbar viele reelle Zahlen darstellen. Stattdessen muß man sich mit einer Teilmenge begnügen. In den meisten Kontexten wird man so genannte **Gleitkommazahlen** benutzen. Diese bestehen aus einem *Vorzeichen*, einer *Mantisse* und einem *Exponenten* in der Form:[^1]

[^1]: Im Kurstext werden Gleitkommazahlen zu beliebigen Basen definiert, wir beschränken uns hier aber auf den Fall der Basis 2.

$$
\pm [0.xxxxxx]_2 \cdot 2^e
$$

Durch die Länge der Mantisse und das Intervall $[e_\ast, e^\ast]$ der möglichen Exponenten ist eine Menge von Gleitkommazahlen eindeutig definiert. In der Praxis sind einige dieser Mengen durch das IEEE als Standards festgelegt worden.

Die Menge aller Gleitkommazahlen, deren erste Mantissenstelle (d.h. die erste Nachkommastelle) nicht Null ist, zusammen mit der Zahl 0, bildet die Menge der **normalisierten** Gleitkommazahlen. Die kleinste positive normalisierte Zahl ist dann $2^{e_\ast-1}$ und die größte knapp unter $2^{e^\ast}$. Bei normalisierten Zahlen besteht jedoch zwischen der kleinsten positiven und der größten negativen Zahl eine größere Lücke, die sich mit Übergang zu den **denormalisierten** Gleitkommazahlen füllen läßt, bei denen die erste Mantissenstelle auch 0 sein kann. Allerdings haben denormalisierte Zahlen folgende Nachteile:

- Dieselbe Zahl kann mehrere Darstellungen haben (z.B. $[0.1]_2\cdot 2^1 = [0.01]_2\cdot 2^2$)
- Die Rundungsfehlerabschätzungen (s.u.) gelten für diese nicht mehr

### Rundung
Reelle Zahlen, die größer als die größte bzw. kleiner als die kleinste Gleitkommazahl sind, können nicht dargestellt werden, man spricht dann von einem **Gleitkommaüberlauf**. Alle anderen Zahlen werden auf die nächstliegende Gleitkommazahl gerundet. Wird eine Zahl, die nicht 0 ist, auf 0 gerundet, spricht man von einem **Gleitkommaunterlauf**.

Es zeigt sich, daß der relative Rundungsfehler bei normalisierten Gleitkommazahlen mit einer Mantisse der Länge $m$ betragsmäßig immer maximal $2^{-m} =: \operatorname{eps}$ beträgt. Mit anderen Worten: die Rundung $\mathrm{rd}(z)$ von $z$ erfüllt die Gleichung $\mathrm{rd}(z) = (1+\varepsilon)z$ mit $\vert\varepsilon\vert\leq\mathrm{eps}$.

Auch bei der Berechnung von Grundrechenarten im Prozessor entstehen Rundungsfehler. Hier zeigt sich, daß stets $a+b = (1+\varepsilon)(a+b)$ mit $\vert\varepsilon\vert \leq \mathrm{eps}$ und dasselbe auch für Subtraktion, Multiplikation und Division gilt. Für elementare Funktionen $f$ (wie $\sin, \cos$, Wurzelfunktion, etc.) beträgt das Ergebnis der Auswertung $(1+\varepsilon)f(x)$, wobei $\vert\varepsilon\vert \leq K_f\mathrm{eps}$ gilt für eine moderate, von $f$ abhängige Konstante $K_f$.

## Fehlerfortpflanzung

### Einführungsbeispiel: Numerische Differentiation

Die Ableitung einer Funktion an einer bestimmten Stelle läßt sich meist nicht exakt bestimmen, selbst nicht bei exakter Arithmetik. Wir können daher versuchen, sie durch den Differenzenquotienten für ein festes $h$ anzunähern:

$$
f^\prime(x) \approx \frac{f(x+h)-f(x)}{h}
$$

Unabhängig von der Maschinenarithmetik können wir anhand des Satzes von Taylor erkennen, daß der **Approximationsfehler** proportional zu $h$ ist. Es ist also naheliegend, $h$ so klein wie möglich zu wählen und zu hoffen, daß dadurch der Fehler möglichst gering wird. Leider funktioniert das in der Praxis nicht: Der Fehler wird zunächst kleiner, wird $h$ aber weiter verringert, vergrößert sich der Fehler wieder. Der Grund dafür ist, daß wir durch die Verwendung von Gleitkommazahlen auch den **Rundungsfehler** berücksichtigen müssen. Dieser ist proportional zu $\frac{\mathrm{eps}}{h}$. Da sich der Gesamtfehler aus der Summe zwischen Approximations- und  Rundungsfehler ergibt, können wir diesen möglichst klein halten, wenn beide Fehler von derselben Größenordnung sind, also $h \approx \frac{\operatorname{eps}}{h}$, also $h \approx \sqrt{\mathrm{eps}}$.

### Kondition von Aufgaben

Wir wollen systematisch untersuchen, wie sich Fehler in Eingabedaten auf den Fehler eines daraus berechneten Resultates einwirken. Speziell untersuchen wir Funktionen $f: \mathbb{R}^n \rightarrow \mathbb{R}$ und einen Eingabevektor $x + \Delta_x$, der durch $\Delta_x$ gestört ist. In linearer Näherung gilt für den absoluten Fehler $\Delta_f = f(x+\Delta_x)-f(x)$

$$
\Delta_f = \sum_{i=1}^n \frac{\partial f}{\partial x_i}(x)\Delta_{x_i}
$$

und für den relativen Fehler $\varepsilon_f = \frac{\Delta_f}{f(x)}$

$$
\varepsilon_f = \sum_{i=1}^n \frac{x_i}{f(x)}\frac{\partial f}{\partial x_i}(x)\varepsilon_{x_i}.
$$

Somit ist der absolute bzw. relative Fehler jeweils eine gewichtete Summe der absoluten bzw. relativen Fehler der Argumente. Speziell die Zahl

$$
\kappa_i := \frac{x_i}{f(x)}\frac{\partial f}{\partial x_i}(x)
$$

wird die **Konditionszahl** bezüglich des i-ten Argumentes genannt. Sind alle Konditionszahlen einer Funktion moderat, sprechen wir von einer **gut konditionierten** Aufgabe, d.h. Fehler in den Eingabedaten wirken sich nur minimal auf das Gesamtergebnis aus.

Bei der Untersuchung der Grundrechenarten stellt sich heraus, daß Multiplikation und Division gut konditioniert sind. Bei der Addition / Subtraktion ist dies anders. Für die Funktion $f(x_1, x_2) = x_1 + x_2$ sind die Konditionszahlen

$$
\kappa_1 = \frac{x_1}{x_1+x_2}, \quad \kappa_2 = \frac{x_2}{x_1+x_2}
$$

Diese werden sehr groß, wenn $x_1 \approx -x_2$. Die Aufgabe, zwei ungefähr gleich große Zahlen voneinander zu subtrahieren, ist also schlecht konditioniert und man muß damit rechnen, daß das Ergebnis nicht brauchbar ist. Untersucht man dies genauer, so sieht man, daß bei Übereinstimmung der ersten $n$ Stellen der beiden Zahlen im Resultat die letzten $n$ Stellen verschwinden. Dieses Phänomen ist als **Stellenauslöschung** bekannt.

Schließlich sehen wir, daß auch das Ziehen einer Wurzel gut konditioniert ist, ebenso wie die Berechnung einer Nullstelle $\lambda = p - \sqrt{p^2-q}$ des Polynoms $\lambda^2-2p\lambda+q$ unter der Voraussetzung $q \ll p^2$.

### Kondition von Algorithmen

Um eine bestimmte Berechnung durchzuführen, wird in der Regel ein Algorithmus verwendet. Bei diesem tauchen Zwischenresultate auf, die ebenfalls fehlerbehaftet sein können und sich so auf das Resultat auswirken. Selbst wenn eine Aufgabe als solche gut konditioniert ist, kann es sein, dass durch die ungünstige Wahl eines Algorithmus das Ergebnis unbrauchbar wird. In diesem Fall bezeichnen wir den Algorithmus als **numerisch instabil**. Auch die Kondition eines Algorithmus kann man systematisch untersuchen, was wir anhand des Beispiels $z = p - \sqrt{p^2-q}$ mit $q \ll p^2$  tun.

Dazu sei der folgende Algorithmus gegeben:

\begin{align*}
y_1 &= p\cdot p\\
y_2 &= y_1 - q\\
y_3 &= \sqrt{y_2}\\
z &= p-y_3
\end{align*}

Wir können nun jeweils die $n$-te **Restabbildung** definieren als die Funktion $R_n: (x,y_1,\ldots,y_n) \mapsto z$ die das Ergebnis aus den Eingabedaten und den ersten $n$ Zwischenergebnissen berechnet. In unserem Beispiel gilt:

\begin{align*}
R_1(p, q, y_1) &= p - \sqrt{y_1 - q}\\
R_2(p, q, y_1, y_2) &= p - \sqrt{y_2}\\
R_3(p, q, y_1, y_2, y_3) &= p - y_3
\end{align*}

Die Konditionszahlen des Algorithmus sind dann die Zahlen

$$
K_i = \frac{y_i}{z}\frac{\partial R_i}{\partial y_i}(x,y)
$$

und der Algorithmus ist genau dann numerisch stabil, wenn alle Konditionszahlen moderat sind.

In unserem Beispiel ist $y_3 \approx p$, also werden im letzten Schritt zwei ungefähr gleich große Zahlen voneinander abgezogen. Somit tritt Stellenauslöschung aus, weswegen $K_3$ sehr groß wird und der Algorithmus instabil ist. Da die Aufgabe allerdings gut konditioniert ist, ist es möglich einen numerisch stabilen alternativen Algorithmus zu finden.

## Lernziele {-}

### Definitionen {-}
- absoluter und relativer Fehler
- Gleitkommazahlen (normalisert, denormalisiert)
- Gleitkommaüber- und -unterlauf, Rundung
- Rundungseigenschaften typischer Operationen / Funktionen
- Stellenauslöschung
- Konditionszahlen von Aufgaben
- Konditionszahlen typischer Problemstellungen (Grundrechenarten, Wurzeln)
- Konditionszahlen von Algorithmen

### Beweise {-}
- Herleitung Rundungsfehler: suche eindeutiges $e$, sodaß Wert in $[2^{e-1}, 2^e)$
- Herleitung Approximations- und Rundungsfehler bei Differentiation: Satz von Taylor
- Herleitung Konditionszahlen: Bildung einer Funktion $\varphi: [0,1]\rightarrow [x, x+\Delta_x]$

### Übungen {-}
- 1.2.9: Distributivgesetz gilt nicht bei Maschinenarithmetik
- 1.3.2: Weitere Übung zu Fehlern bei Differentiation
- Studientagsaufgabe 2: Bestimmen von Konditionszahlen
    - Für kleine x -> Grenzwert betrachten!
    - Bei Herleitung eines stabilen Algorithmus: Stellenauslöschung vermeiden!
- EA 1: Fehlerfortpflanzung bei Integralen
- EA 3: Rundungsfehler bei verschiedenen Berechnungen von $x^2-y^2$

