---
previous_sections: 7
---
# Nichtlineare Gleichungen

In diesem Kapitel diskutieren wir Verfahren zum Lösen nichtlinearer Gleichungen.

## Zusammenfassung {-}

## Fixpunkt- und Nullstellenaufgaben

Die beiden Aufgabentypen, die wir betrachten wollen, sind:

- **Fixpunktaufgabe**: Gegeben eine Funktion $f$, suche ein $x$ mit $f(x) = x$.
- **Nullstellenaufgabe**: Gegeben eine Funktion $f$, suche eine Nullstelle von $f$

Die beiden Aufgabentypen können ineinander übergeführt werden: So lässt sich die Fixpunktaufgabe $f(x) = x$ in $g(x) = 0$ für $g = f - \mathrm{id}$ umschreiben und $f(x) = 0$ ist äquivalent zu $g(x) = x$ für $g = f + \mathrm{id}$.

## Fixpunktiteration
Wir erinnern an den Banach'schen Fixpunktsatz: Sei dazu $D \subset \mathbb{R}^n$ abgeschlossen und $f$ auf $D$ eine kontrahierende Selbstabbildung, d.h. $f(x) \in D$ für alle $x \in D$ und $\lVert f(x) - f(y) \rVert \leq \alpha \lVert x - y \rVert$ für alle $x, y \in D$ mit einem $\kappa < 1$. Dann hat $f$ genau einen Fixpunkt $x^\ast$. Wählen wir ein $x_0 \in D$ und setzen jeweils $x_i = f(x_{i-1})$, so konvergiert die entsprechende Folge zu $x^\ast$.

Wichtig sind in diesem Zusammenhang auch die folgenden Gleichungen:

- $\lVert x_k - x^\ast \rVert \leq \alpha \lVert x_{k-1}-x^\ast\lVert$. Dies zeigt, daß sich die Näherung stets verbessert.
- $\lVert x_k -x^\ast \rVert \leq \frac{\alpha^k}{1-\alpha}\lVert x_0 - x_1 \rVert$. Damit können wir **a priori** abschätzen, wieviele Iterationsschritte wir benötigen, um eine bestimmte Genauigkeit zu erreichen.
- $\lVert x_k - x^\ast \lVert \leq \frac{\alpha}{1-\alpha}\lVert x_k - x_{k-1} \rVert$. Damit können wir **a posteriori** abschätzen, welche Genauigkeit wir beim aktuellen Iterationsschritt erreicht haben.

Ist die Kontraktionskonstante nicht bekannt, so kann diese bspw. mit $\alpha \approx \frac{\lVert x_{k+1}-x_k\rVert}{\lVert x_k-x_{k-1}\rVert}$ geschätzt werden. Ist dieser Näherungswert kleiner als 1, ist jedoch nicht automatisch garantiert, daß auch $\alpha < 1$ gilt.

Ist bereits bekannt, daß eine Funktion einen Fixpunkt $x^\ast$ besitzt, so sagt der folgende Satz aus, daß die Iteration in einer Umgebung von $x^\ast$ unter bestimmten Voraussetzungen konvergiert. Gilt nämlich für die Ableitung (Jacobi-Matrix) für eine beliebige Norm $\lVert f^\prime(x^\ast)\lVert < 1$, und ist $f$ stetig differenzierbar, so gilt für jedes $\alpha$ mit $\lVert f^\prime(x^\ast) \rVert < \alpha < 1$, daß $f$ in einer Umgebung von $x^\ast$ eine kontrahierende Selbstabbildung mit Kontraktionskonstante $\alpha$ ist.

## Konvergenzgeschwindigkeit

Sei eine Folge $(x_i)$ gegeben, die gegen $x^\ast$ konvergiert. Für die Folgen $\varepsilon_i := \lVert x_i - x^\ast \rVert$ wollen wir untersuchen, wie schnell sie jeweils gegen $0$ konvergieren. Dafür führen wir verschiedene Sprechweisen ein, insbesondere:

- $\limsup_{k\to\infty}\frac{\varepsilon_k}{\varepsilon_{k-1}} = \alpha < 1$: Die Folge konvergiert **Q-linear** mit Kontraktionskonstante $\alpha$
- $\lim_{k\to\infty} \frac{\varepsilon_k}{\varepsilon_{k-1}} = 0$: Die Folge konvergiert **Q-überlinear**
- $\limsup_{k\to\infty} \frac{\varepsilon_k}{\varepsilon_{k-1}^p} \in \mathbb{R}$: Die Folge konvergiert mit **Q-Ordnung** $p$ (für $p > 1$), z.B. sprechen wir auch von **Q-quadratisch**

## Lösen von Nullstellenaufgaben

Wir wenden uns nun Lösungsverfahren für Nullstellenaufgaben zu.

### Bisektion
Das Bisektionsverfahren ist sehr simpel und funktioniert nur im eindimensionalen Fall. Hierzu gehen wir davon aus, daß wir Stellen $a$ und $b$ haben, für die $f(a)$ und $f(b)$ entgegengesetzte Vorzeichen haben. Wir wählen nun den Mittelpunkt $m$ zwischen $a$ und $b$ und betrachten $f(m)$. Ist $f(m) = 0$, so sind wir fertig. Andernfalls verkleinern wir unser Suchintervall: Hat $f(m)$ dasselbe Vorzeichen wie $f(a)$, so ist unser neues Suchintervall $[m, b]$, ansonsten ist das neue Intervall $[a,m]$.

### Newton-Verfahren
Die Idee des Newton-Verfahrens basiert auf der linearen Näherung einer differenzierbaren Funktion:
$$
f(x^\ast) \doteqdot f(x) + f^\prime(x)(x-x_k)
$$
Da $f(x^\ast) = 0$ gilt, lässt sich diese Gleichung nach $x^\ast$ umstellen, um so, ausgehend von einem Startwert $x$, eine Näherung für die Nullstelle zu erhalten. Iterieren wir dieses Verfahren, so erhalten wir, ausgehend von einem Startwert $x_0$ die Iterationsvorschrift
$$
x_{k+1} = x_k-\frac{f(x_k)}{f^\prime(x_k)}
$$
bzw. im mehrdimensionalen Fall
$$
x_{k+1} = x_k-(f^\prime(x_k))^{-1}f(x_k).
$$
In der Tat wir man in diesem zweiten Fall jedoch nicht die Inverse einer Matrix bestimmen, sondern eher das Gleichungssystem $f^\prime(x_k)s = -f(x_k)$ lösen und dann $x_{k+1} = x_k + s$ setzen.

Hat die Funktion $f$ eine Nullstelle $x^\ast$, an der die Ableitung invertierbar ist, und ist $f$ in einer Umgebung von $x^\ast$ Lipschitz-stetig, so existiert eine Umgebung, aus der man einen Startwert $x_0$ so wählen kann, sodaß das Verfahren Q-quadratisch konvergiert.

Allerdings kann diese Umgebung zu klein sein, um in der Praxis von Nutzen zu sein. Als Abhilfe betrachtet man das **gedämpfte Newton-Verfahren**: Bei diesem wird ausgenutzt, daß $f(x^\ast) = 0$ äquivalent zu $\lVert f(x^\ast) \rVert = 0$ ist und versucht nun, in jedem Schritt die Norm des Funktionswertes zu verringern.

Sind ein Punkt $x$ und ein Vektor $s$ gegeben und existiert ein $t > 0$, sodaß $\lVert f(x+t^\ast s) \rVert < \lVert f(x) \rVert$ für alle $0 < t^\ast < t$ gilt, so nennen wir $s$ eine **Abstiegsrichtung**. Man kann nun zeigen, daß $s := -(f^\prime(x_k))^{-1}f(x_k)$ jeweils eine Abstiegsrichtung ist, falls die Ableitung invertierbar ist und $f$ auf dem Definitionsbereich Lipschitz-stetig ist. Statt diesen Vektor wie beim normalen Newton-Verfahren komplett von $x_k$ abzuziehen, wird man nun wie folgt vorgehen: Zunächst setze man $\gamma = 1$. Dann prüfe man, ob $\lVert f(x+\gamma s) \rVert < \lVert f(x) \rVert$ gilt. Ist dies der Fall, so setzt man $x_{k+1} = x+\gamma s$. Ansonsten, setze man $\gamma := \frac{\gamma}{2}$ und fahre weiter wie zuvor, bis man ein $\gamma$ erhält, das die Bedingung erfüllt. Durch diese Modifikation läßt sich das Verfahren unter den gegebenen Voraussetzungen stets durchführen.

### Sekantenverfahren

Bei der Umsetzung des Newton-Verfahrens ist es nicht immer möglich oder ratsam, die Ableitung $f^\prime(x_k)$ auszurechnen. Im eindimensionalen Fall kann man diese stattdessen durch einen Differenzenquotienten annähern. Geben wir also zwei Startwerte $x_0, x_1$ vor, so erhalten wir die Iterationsvorschrift
$$
x_{k+1} = x_k - \frac{x_k-x_{k-1}}{f(x_k)-f(x_{k-1})}f(x_k)
$$

Es zeigt sich, daß dieses Verfahren lokal Q-linear konvergiert.

## Lernziele {-}
### Definitionen und Sätze {-}
- Fixpunktaufgabe, Nullstellenaufgabe
- Satz 8.1.5: Lokale Existenz einer kontrahierenden Selbstabbildung
- Konvergenzgeschwindigkeiten

### Verfahren {-}
- Konvertieren von Fixpunkt- und Nullstellenaufgaben ineinander
- Banach'sches Fixpunktverfahren
- Bisektionsverfahren
- Newton'sches Verfahren (ein- und mehrdimensional)
- gedämpftes Newton-Verfahren
- Sekantenverfahren

### Beweise {-}
- Q-quadratische Konvergenz des Newton-Verfahrens (eindimensional)

### Übungen {-}
- 8.1.3: Ungleichungen zu kontrahierenden Abbildungen
- 8.3.3: Abschätzungen zur Konvergenz des Sekantenverfahrens
- Studientag:
    - 18: Fixpunktiteration
    - 19: Newton- und Sekantenverfahren