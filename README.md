# Notizen Numerik

Notizen zum Kurs Numerische Mathematik 1 an der Fernuniversität in Hagen.

[Link zum aktuellsten PDF](https://gitlab.com/pfrasa/notizen-numerik/-/jobs/artifacts/master/file/dist/notes.pdf?job=build-pdf).
